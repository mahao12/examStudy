## 目录
### 1.MARKDOWN 
### 2.TypeScript
_____________________________________________________________________________

### 1.MARKDOWN 
####  
[基本撰写和格式语法](https://docs.github.com/cn/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)  
[Markdown 官方教程](https://markdown.com.cn/basic-syntax/line-breaks.html)
### 2.TypeScript

1. TypeScript 基础类型
 number,string,undefined,boolean,
2. TypeScript 变量声明
let flag: boolean = true;
let arr: number[] = [1, 2];
var uname:string = "Runoob";
3. TypeScript 运算符    
    双目运算符  0<1?0:1  
    ||  
    !
    &&   
--  
    ++  

4. TypeScript 条件语句
```
var  num:number = 5
if (num > 0) { 
   console.log("num>0") 
}
```
5. TypeScript 循环  
```
var num:number = 5; 
var i:number; 
var temp = 1; 
 
for(i = num;i>=1;i--) {
   temp *= i;
}
console.log(temp)
```

6. TypeScript 函数  
```
function () {   
    
    console.log("调用") 
}
// 匿名函数定义
——————
const b= function a(){
    console.log("p")
}
b()
————————

````

7. TypeScript Array(数组)
```
// filter
// every
// map
// reduce
//slice
//splice
```

```
 let a:number=9
 let b:string= 'sd'
 const c:number=90
 const f:number[]=[2,3.4,4]
 const e:{key:string}={key:'val'}
// if 
if(c==90){
    console.log('ok')
}

// for 

//加减乘除运算符，取余
let i=9
// console.log(i++)
// console.log(i)
// console.log(i--)
// console.log(i)
// console.log(i%2)
// console.log(i)

// 三元运算符
const m=0<1?true:false
console.log(m)

// 
while(i<10){
i++;
console.log(i)
}
// == 和===
// 判断类型和值

// 函数,?:即undefined
function exam(a:number,v:number,d?:string,v1=false){
console.log("ok")
console.log(a+v)

}
 exam(1,9);
// 匿名函数***
// const A=function (){

// }
// (function (){
//     console.log(a+"ok")
// })()
// 箭头函数
const a1=()=>{
    console.log("okk")
}
a1();

const a2=()=>2;
const b2= a2();
console.log(b2)

function a3():number{

    return 1;
    
}
// const a4=():number=>{}

const n=[2,3,4]
 n.pop();
// 删除
n.push(7);
// 增加
console.log(n)
n.unshift(4);
n.shift()
console.log (n)
// 从头部添加，删除，改变原数组



// 切割,取头不取尾
n.slice(0,2)
// splice 会改变原本的数组,删除
console.log(n.splice(0,2))
console.log(n)
n.push(3,4,5)
console.log(n)
// // 
// const  aa:Array<Number|string> =[1,23,"we"];
const  aa:(Number|string)[]=[1,23,"we"];
// console.log(aa.includes("wed"))
// console.log(aa.indexOf(23))
// console.log(aa.join("_"))
// forEach
//  aa.forEach((val,inde,array)=>{
//      console.log(val,2,aa)
//  })

//  aa.find((val,inde,array)=>{
//      console.log(val,inde,array)
//  })
// aa.findIndex(()=>{})
const  aa1:(Number)[]=[1,2,3,4];
const bb= aa1.every((val,inde,array)=>{
    val>1

})
console.log(bb)
// filter
// every
// map
// reduce
```
